package sample;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.FileChooser;
import javafx.beans.binding.Bindings;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    MediaPlayer mediaPlayer;
    Media media;
    @FXML
    private MediaView myMediaView;
    @FXML
    private Button playButton;
    @FXML
    private Button playPrevious;
    @FXML
    private Button playNext;
    @FXML
    private Slider timeSlider;


    @FXML
    void exitWindow(ActionEvent event) {
        Platform.exit();

    }

    @FXML
    void openAboutWindow(ActionEvent event) throws IOException {
    try {
        Parent root = FXMLLoader.load(getClass().getResource("About.fxml"));
        Stage stage = new Stage();
        stage.setTitle("About");
        stage.setResizable(false);
        stage.setScene(new Scene(root, 700, 475));
        stage.show();
    }
    catch(Exception e){

    }
    }

    @FXML
    void backwardMedia(ActionEvent event) {
        double d=mediaPlayer.getCurrentTime().toSeconds();
        d=d-10;
        mediaPlayer.seek(new Duration(d*1000));
    }

    @FXML
    void forwardMedia(ActionEvent event) {
        double d=mediaPlayer.getCurrentTime().toSeconds();
        d=d+10;
        mediaPlayer.seek(new Duration(d*1000));
    }

    @FXML
    void playMenu(ActionEvent event) {
        if(media!=null){

                mediaPlayer.play();


            setResponsive();
        }

    }
    @FXML
    void pauseMenu(ActionEvent event) {
      MediaPlayer.Status status= mediaPlayer.getStatus();
       if(status== MediaPlayer.Status.PLAYING){
           mediaPlayer.pause();
       }

    }

    @FXML
    void openSong(ActionEvent event) {
        try {
            FileChooser fileChooser = new FileChooser();
            File file = fileChooser.showOpenDialog(null);
            media = new Media(file.toURI().toString());
            if(mediaPlayer!=null){
                mediaPlayer.dispose();
            }
            mediaPlayer = new MediaPlayer(media);
            myMediaView.setMediaPlayer(mediaPlayer);

            mediaPlayer.setOnReady(()->{
                timeSlider.setMin(0);
                timeSlider.setMax(mediaPlayer.getMedia().getDuration().toMinutes());
                timeSlider.setValue(0);
            });

            //listener on media player
            mediaPlayer.currentTimeProperty().addListener(new ChangeListener<Duration>() {
                @Override
                public void changed(ObservableValue<? extends Duration> observable, Duration oldValue, Duration newValue) {
                    Duration duration=mediaPlayer.getCurrentTime();
                    timeSlider.setValue(duration.toMinutes());
                }
            });

//            //listener on timeSlider
                timeSlider.valueProperty().addListener(new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                        if(timeSlider.isPressed()){
                            double val=timeSlider.getValue();
                            mediaPlayer.seek(new Duration(val*60000));

                        }
                    }
                });
        }
        catch (Exception e){

        }

    }
    @FXML
    void playMedia(ActionEvent event) {
      try{
          if(media!=null) {
              setResponsive();
              MediaPlayer.Status status = mediaPlayer.getStatus();
              if (status == MediaPlayer.Status.PLAYING) {
                  //pause
                  mediaPlayer.pause();
                  playButton.setGraphic(new ImageView(new Image(new FileInputStream("src/icons/play.png"))));
              } else {
                  mediaPlayer.play();
                  playButton.setGraphic(new ImageView(new Image(new FileInputStream("src/icons/pause.png"))));
              }
          }
      }
      catch (Exception exception){
          exception.printStackTrace();
      }
        finally {

      }

    }

    void setResponsive()
    {
        DoubleProperty width=myMediaView.fitWidthProperty();
        DoubleProperty height=myMediaView.fitHeightProperty();
        width.bind(Bindings.selectDouble(myMediaView.sceneProperty(),"width"));
        height.bind(Bindings.selectDouble(myMediaView.sceneProperty(),"height"));
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try{
            playButton.setGraphic(new ImageView(new Image(new FileInputStream("src/icons/play.png"))));
            playNext.setGraphic(new ImageView(new Image(new FileInputStream("src/icons/next.png"))));
            playPrevious.setGraphic(new ImageView(new Image(new FileInputStream("src/icons/previous.png"))));
        }
        catch(Exception exception){

        }
    }
}
